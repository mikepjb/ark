(defvar markdown-mode-hook nil)

(defvar markdown-mode-map
  (let ((map (make-keymap)))
    map)
  "Keymap for Markdown major mode")

(defconst markdown-font-lock-keywords
  (list
   `(,(concat "\\b" (regexp-opt '("-") t) "\\b") . 'font-lock-keyword-face)
   `(,(concat "\\b" (regexp-opt '("*") t) "\\b") . 'font-lock-type-face)
   )
  "Minimal highlighting expressions for Markdown mode")

(defvar markdown-mode-syntax-table
  (let ((st (make-syntax-table)))
    st)
  "Syntax table for Markdown mode")

(define-derived-mode markdown-mode prog-mode "Markdown"
  "Major mode for editing Markdown Language files"
  (set (make-local-variable 'font-lock-defaults) '(markdown-font-lock-keywords)))

(provide 'markdown-mode)

(defvar yaml-mode-hook nil)

(defvar yaml-mode-map
  (let ((map (make-keymap)))
    map)
  "Keymap for Yaml major mode")

(defconst yaml-font-lock-keywords
  (list
   `(,(concat "\\b" (regexp-opt '("-") t) "\\b") . 'font-lock-type-face)
   `("\\w*:" . 'font-lock-keyword-face))
  "Minimal highlighting expressions for Yaml mode")

(defvar yaml-mode-syntax-table
  (let ((st (make-syntax-table))) st)
  "Syntax table for Yaml mode")

(define-derived-mode yaml-mode prog-mode "YAML"
  "Major mode for editing Yaml Language files"
  (set (make-local-variable 'font-lock-defaults) '(yaml-font-lock-keywords)))

(provide 'yaml-mode)

(defvar r-mode-hook nil)

(defvar r-mode-map
  (let ((map (make-keymap)))
    ;; (define-key map (kbd "C-j") 'newline-and-indent)
    map)
  "Keymap for R major mode")

(defconst r-font-lock-keywords
  (list
   `(,(regexp-opt '("source" "read.csv" "names" "is.na" "nchar" "function" "rbind") t) . 'font-lock-builtin-face)
   `(,(regexp-opt '("if" "else" "%>%" "%in%" "<-" "->") t) . 'font-lock-keyword-face)
   `(,(regexp-opt '("NA") t) . 'font-lock-type-face)
   )
  "Minimal highlighting expressions for R mode")

(defvar r-mode-syntax-table
  (let ((st (make-syntax-table)))
    ;; Set comment syntax
    (modify-syntax-entry ?# "< 1" st)
    (modify-syntax-entry ?\n "> b" st)
    
    (modify-syntax-entry ?_ "w" st)
    (modify-syntax-entry ?' "\"" st)
    (modify-syntax-entry ?\" "\"" st)
    st)
  "Syntax table for R mode")

(define-derived-mode r-mode prog-mode "R"
  "Major mode for editing R Language files"
  (set (make-local-variable 'font-lock-defaults) '(r-font-lock-keywords)))

(provide 'r-mode)


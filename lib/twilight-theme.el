(deftheme twilight "Code illuminated.")

(let ((class '((class color) (min-colors 89)))
      (twilight-black "#1b1d1e")
      (twilight-grey "#333333")
      (twilight-white "#eeeeee")
      (twilight-lime "#80ff00")
      (twilight-cream "#ffff99")
      (twilight-blue "#6bd9ed")
      (twilight-green "#b9e665")
      (twilight-magenta "#ff66a3")
      (twilight-purple "#e699ff")
      (twilight-region "#333300")
      (temp-yellow "#ffcc00"))

  (custom-theme-set-faces
   'twilight
   `(default ((,class (:foreground ,twilight-white :background ,twilight-black))))
   `(cursor ((,class (:background ,temp-yellow))))
   `(region ((,class (:background ,twilight-region))))
   `(highlight ((,class (:background ,twilight-grey))))
   `(font-lock-builtin-face ((,class (:foreground ,twilight-magenta))))
   `(font-lock-keyword-face ((,class (:foreground ,twilight-magenta))))
   `(font-lock-type-face ((,class (:foreground ,twilight-green))))
   `(font-lock-function-name-face ((,class (:foreground ,twilight-green))))
   `(font-lock-string-face ((,class (:foreground ,twilight-cream))))
   `(font-lock-comment-face ((,class (:foreground ,twilight-purple))))
   `(comint-highlight-prompt ((,class (:foreground ,twilight-blue))))
   `(diff-added ((,class (:foreground ,twilight-green))))
   `(diff-removed ((,class (:foreground ,twilight-magenta))))
   `(diff-hunk-header ((,class (:foreground ,twilight-blue))))
   `(diff-file-header ((,class (:foreground ,twilight-blue))))
   `(diff-header ((,class (:foreground ,twilight-blue))))
   `(ido-first-match ((,class (:foreground ,twilight-green))))
   `(ido-only-match ((,class (:foreground ,twilight-green))))
   `(ido-subdir ((,class (:foreground ,twilight-cream))))
   `(ido-indicator ((,class (:foreground ,twilight-green))))
   `(match ((,class (:background ,twilight-region))))
   `(compilation-info ((,class (:foreground ,twilight-green))))
   (custom-theme-set-variables 'twilight
    `(ansi-color-names-vector
      [,twilight-black
       ,twilight-magenta ;; red
       ,twilight-green
       ,twilight-cream ;; yellow
       ,twilight-blue
       ,twilight-magenta
       ,twilight-blue ;; cyan
       ,twilight-white]))
   `(minibuffer-prompt ((,class (:foreground ,twilight-blue))))
   ))

(provide-theme 'twilight)

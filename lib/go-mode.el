(defvar go-mode-hook nil)

(defvar go-mode-map
  (let ((map (make-keymap)))
    map)
  "Keymap for Go major mode")

(defconst go-mode-keywords
  '("break" "default" "func" "interface" "select"
    "case" "defer" "go" "map" "struct" "chan" "else"
    "goto" "package" "type"))

(defconst go-font-lock-keywords
  (list
   `(,(regexp-opt '("package" "import" "type" "func" "len") t) . 'font-lock-builtin-face)
   `(,(concat "\\b" (regexp-opt go-mode-keywords t) "\\b") . 'font-lock-keyword-face)
   `(,(concat "\\b" (regexp-opt '("bool" "string" "nil") t) "\\b") . 'font-lock-type-face)
   )
  "Minimal highlighting expressions for Go mode")

(defconst go-identifier-regexp "[[:word:][:multibyte:]]+")
(defconst go-label-regexp go-identifier-regexp)

(defvar go-mode-syntax-table
  (let ((st (make-syntax-table)))
    ;; Set comment syntax
    (modify-syntax-entry ?\/ ". 124b" st)
    (modify-syntax-entry ?\n "> b" st)
    st)
  "Syntax table for Go mode")

;; mostly works.
;; basic logic is to look backwards until you find a (, {, ) or } without
;; a pair e.g () and increment/decrement the indentation relative to that
(defun go-indent-line ()
  "Indent current line as go code"
  (interactive)
  (if (bobp)
      (indent-line-to 0)
    (let ((not-indented t) cur-indent)
      (if (looking-at "\\()\\|}\\)$")
          (progn
            (save-excursion
              (forward-line -1)
              (setq cur-indent (- (current-indentation) tab-width)))
            (if (< cur-indent 0)
                (setq cur-indent 0)))
        (save-excursion
          (while not-indented ;; iterate backward until we find an indent hint
            (forward-line -1)
            (if (looking-at ".*\\()\\|}\\)") ;; closing scope hint
                (progn
                  (setq cur-indent (current-indentation))
                  (setq not-indented nil))
              (if (and (looking-at ".*\\((\\|{\\)")
                       (not (looking-at ".*\\()\\|}\\)")))
                  (progn
                    (setq cur-indent (+ (current-indentation) tab-width))
                    (setq not-indented nil))
                (if (bobp)
                    (setq not-indented nil)))))))
      (if cur-indent
          (indent-line-to cur-indent)
        (indent-line-to 0)))))

(define-derived-mode go-mode prog-mode "Go"
  "Major mode for editing Go Language files"
  (set (make-local-variable 'font-lock-defaults) '(go-font-lock-keywords))
  (set (make-local-variable 'indent-line-function) 'go-indent-line)

  (set (make-local-variable 'comment-start) "// ")
  (set (make-local-variable 'comment-end)   "")
  (set (make-local-variable 'comment-use-syntax) t)
  (set (make-local-variable 'comment-start-skip) "\\(//+\\|/\\*+\\)\\s *"))

(provide 'go-mode)

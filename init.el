(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

(setq-default
 backup-directory-alist '(("" . "~/.emacs.d/backup"))
 column-number-mode t
 custom-theme-load-path (list "~/.emacs.d/lib")
 ido-auto-merge-work-directories-length nil
 ido-enable-flex-matching t
 ido-everywhere t
 indent-tabs-mode nil
 inhibit-startup-screen t
 mac-command-modifier 'meta
 ns-use-native-fullscreen nil
 ring-bell-function 'ignore
 scroll-step 1
 tab-width 2
 vc-follow-symlinks t
 package-enable-at-startup nil)

(set-frame-font "Inconsolata Bold 19" nil t)

(when window-system
  (set-frame-size (selected-frame) 160 55)
  (setq default-frame-alist
        '((width . 160) (height . 55) (vertical-scroll-bars . nil))))

(load-theme 'twilight t)

(defun kill-region-or-backward-word ()
  (interactive)
  (if mark-active
      (kill-region (region-beginning) (region-end))
    (if (bound-and-true-p paredit-mode)
        (paredit-backward-kill-word)
      (backward-kill-word 1))))

(defun git-root ()
  (let ((response (shell-command-to-string
                   "echo -ne $(git rev-parse --show-toplevel || echo \".\")")))
    (if (string-match-p (regexp-quote "fatal") response) "." response)))

(defun ido-search ()
  (interactive)
  (save-excursion
    (find-file
     (concat
      (git-root)
      "/"
      (ido-completing-read
       "Open: "
       (split-string
        (shell-command-to-string
         (concat "cd " (git-root) " && find * -type f")) "\n")
       nil
       t)))))

(defun prettify-json ()
  (interactive)
  (let ((b (if mark-active (min (point) (mark)) (point-min)))
        (e (if mark-active (max (point) (mark)) (point-max))))
    (shell-command-on-region b e
     "python -mjson.tool" (current-buffer) t)))

(defun look () (interactive) (rgrep (grep-read-regexp) "*" (git-root)))

(dolist
    (binding
     '(("C-c g P" . vc-push)
       ("C-c g l" . vc-print-root-log)
       ("C-c g p" . vc-pull)
       ("C-c g r" . vc-revert) ;; git reset current file
       ("C-c g s" . vc-dir)
       ("C-c i" . (lambda () (interactive) (find-file "~/.emacs.d/init.el")))
       ("C-t" . ido-search)
       ("C-;" . ido-switch-buffer)
       ("C-c l" . look)
       ("C-c b" . (lambda () (interactive) (split-window-below) (other-window 1) (inferior-lisp "boot repl")))
       ("C-c n" . (lambda () (interactive) (find-file "~/notes/today.md")))
       ("C-c r" . replace-string)
       ("C-c t" . (lambda () (interactive) (message (current-time-string))))
       ("C-x C-v" . vc-dir)
       ("C-h" . delete-backward-char)
       ("C-w" . kill-region-or-backward-word)
       ("M-W" . delete-frame)
       ("C-z" . shell)
       ("M-/" . comment-line-or-region)
       ("M-RET" . toggle-frame-fullscreen)
       ("M-[" . hippie-expand)
       ("M-]" . other-frame)
       ("M-g" . mark-paragraph)
       ("M-j" . (lambda () (interactive) (next-line 1) (join-line)))
       ("M-k" . paredit-forward-barf-sexp)
       ("M-l" . paredit-forward-slurp-sexp)
       ("M-n" . make-frame) ;; may wreck havoc.. check this space
       ("M-o" . other-window)
       ("C-j" . newline)))
  (global-set-key (kbd (car binding)) (cdr binding)))

(global-set-key (kbd "C-c h") help-map)

(defun bind-ido-keys ()
  "Keybindings for ido mode."
  (define-key ido-completion-map (kbd "`") 'ido-exit-minibuffer)
  (define-key ido-completion-map (kbd "C-w") 'ido-delete-backward-word-updir)
  (define-key ido-completion-map (kbd "SPC") 'ido-exit-minibuffer)
  (define-key ido-completion-map (kbd "TAB") 'ido-exit-minibuffer))
(add-hook 'ido-setup-hook #'bind-ido-keys)

(ido-mode t)

(defun bind-comint-keys ()
  (define-key comint-mode-map (kbd "C-l") 'comint-clear-buffer)
  (define-key comint-mode-map (kbd "C-u") 'comint-kill-input))
(add-hook 'comint-mode-hook #'bind-comint-keys)

(defun bind-inferior-lisp-keys ()
  (define-key inferior-lisp-mode-map (kbd "C-l") 'comint-clear-buffer)
  (define-key inferior-lisp-mode-map (kbd "C-u") 'comint-kill-input))
(add-hook 'inferior-lisp-mode-hook #'bind-inferior-lisp-keys)

(defun bind-dired-keys ()
  (define-key dired-mode-map (kbd "C-t") 'ido-search))
(add-hook 'dired-mode-hook #'bind-dired-keys)

(defun bind-ido-keys ()
  "Keybindings for ido mode."
  (define-key ido-completion-map (kbd "`") 'ido-exit-minibuffer)
  (define-key ido-completion-map (kbd "C-w") 'ido-delete-backward-word-updir)
  (define-key ido-completion-map (kbd "SPC") 'ido-exit-minibuffer)
  (define-key ido-completion-map (kbd "TAB") 'ido-exit-minibuffer))
(add-hook 'ido-setup-hook #'bind-ido-keys)

(defun bind-paredit-keys ()
  (define-key paredit-mode-map (kbd "{") 'paredit-open-curly)
  (define-key paredit-mode-map (kbd "}") 'paredit-close-curly))
(add-hook 'paredit-mode-hook #'bind-paredit-keys)

(dolist (mode '(emacs-lisp
                clojure
                inferior-lisp))
  (add-hook (intern (format "%s-mode-hook" mode)) #'enable-paredit-mode))
(add-hook 'eval-expression-minibuffer-setup-hook #'paredit-mode)

(defun display-startup-echo-area-message ()
  (let ((messages
	 '("I used to live by a code."
	   "Learn from your mistakes"
	   "What is broken can be reforged")))
    (message (nth (random (length messages)) messages))))

(if (eq system-type 'darwin)
    (let ((path-from-shell
	   (shell-command-to-string "$SHELL -i -c 'echo $PATH'")))
      (setenv "PATH" path-from-shell)
      (setq exec-path (split-string path-from-shell path-separator))))

(show-paren-mode t)
(electric-pair-mode t)

(add-to-list 'load-path "~/.emacs.d/lib")

(require 'paredit)

(autoload 'r-mode "r-mode" "R major mode" t)
;;;###autoload
(add-to-list 'auto-mode-alist '("\\.\\(r\\|R\\)\\'" . r-mode))

(autoload 'go-mode "go-mode" "golang major mode" t)
;;;###autoload
(add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode))
;; (add-hook 'before-save-hook #'gofmt-before-save)

(autoload 'markdown-mode "markdown-mode" "markdown major mode" t)
;;;###autoload
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

(autoload 'clojure-mode "clojure-mode" "clojure major mode" t)
(autoload 'inf-clojure "inf-clojure" "inf-clojure mode" t)
;;;###autoload
(add-to-list 'auto-mode-alist '("\\.clj\\'" . clojure-mode))

(autoload 'yaml-mode "yaml-mode" "yaml major mode" t)
;;;###autoload
(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))

(setq inferior-lisp-program "boot repl")

(defun fun-code-that-will-not-load-on-startup ()
  (shell-command-to-string "git status")
  )

;; Emacs rules
;; `(,(forced-eval-signalled-via-comma) . '(other-non-eval-stuff))
